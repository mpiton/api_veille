import * as Boom from "@hapi/boom";
import { Server } from "@hapi/hapi";
import * as dotenv from "dotenv";
import { RequestWithMongo } from "./interfaces";

dotenv.config();
export let server: Server;

export const init = async (): Promise<Server> => {
  server = new Server({
    port: process.env.PORT || 4000,
    host: "0.0.0.0",
    routes: {
      cors: {
        origin: ["*"],
        additionalHeaders: ["cache-control", "x-requested-with"],
      },
    },
  });

  await server.register({
    plugin: require("hapi-mongodb"),
    options: {
      url: process.env.MONGO_URL,
      decorate: true,
    },
  });

  //   Routes
  server.route({
    method: "GET",
    path: "/",
    handler: () => {
      return "Hello Stranger!";
    },
  });
  server.route({
    method: "GET",
    path: "/feeds",
    handler: async (request: RequestWithMongo) => {
      try {
        const db = request.mongo.db;
        const feeds = await db
          .collection("tweets")
          .find()
          .sort({ createdAt: -1 })
          .limit(20)
          .toArray();
        return feeds;
      } catch (err) {
        throw Boom.internal("Internal MongoDB error", err);
      }
    },
  });

  return server;
};

export const start = async (): Promise<void> => {
  try {
    await server.start();
    console.log(`Server running at ${server.info.uri} `);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};
